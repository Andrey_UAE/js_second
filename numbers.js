"use strict";

let i;
let myNumber;
do {
    myNumber = prompt("Enter Number");
}
while (isNaN(myNumber) || myNumber === "" || myNumber < 0 || myNumber == null || myNumber % 1 === 1);

if (myNumber % 5 === 0) {
    for (i = 0; i <= myNumber; i += 5) {
        console.log(i)
    }
}
else {
    console.log("Sorry, no numbers")
}


